package com.tg.backend.service;

import com.tg.backend.common.*;
import com.tg.backend.model.Game;
import com.tg.backend.model.Player;
import com.tg.backend.model.units.*;
import com.tg.backend.repository.GameStateRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {


    private UnitService unitService = new UnitService();

    public void executeOrder(Player player, Player enemy, Order order, Game game, GameStateRepository gameStateRepository) {
        boolean isActionCorrect = false;
        for (Action action : order.getActions()) {
            if (action == null) {
                unitService.getLogs().add("wrong action command");
                break;
            }
            switch (action.getActionType()) {
                case ATTACK:
                    isActionCorrect = attack(player, enemy, action.getId(), action.getTarget());
                    break;
                case RECRUIT:
                    isActionCorrect = recruit(player, action.getTarget());
                    break;
                case MOVE:
                    isActionCorrect = move(player, action.getId(), action.getTarget());
                    break;
                case ENTRENCH:
                    isActionCorrect = entrench(player, action.getId());
                    break;
                default:
                    unitService.getLogs().add("wrong action command");
                    break;
            }
            if (!isActionCorrect) {
                break;
            }
            game.setLastLogs(unitService.getLogs());
            gameStateRepository.save(new GameState(game));
            //game.incrementCurrentStep();
            unitService.getLogs().clear();
        }
    }

    public boolean recruit(Player player, String unit) {
        unitService.getLogs().add("recruit");
        Unit recruitedUnit = recruit(unit);
        if (recruitedUnit != null) {
            unitService.concatToLastLog(recruitedUnit.getName());
            if ((player.getBase().getNewUnit() == null) && (recruitedUnit.getCost() <= player.getGold())) {
                player.setGold(player.getGold() - recruitedUnit.getCost());
                recruitedUnit.setCoordinates(player.getBase().getCoordinates());
                player.getBase().setNewUnit(recruitedUnit);
                unitService.concatToLastLog("OK, -" + recruitedUnit.getCost() + " GOLD");
                return true;
            } else if (player.getBase().getNewUnit() != null) {
                player.setGold(Math.max(0, player.getGold() - recruitedUnit.getCost() / 2));
                unitService.concatToLastLog("invalid - BASE is not empty, -" + (recruitedUnit.getCost() / 2) + " GOLD");
                return false;
            } else {
                unitService.concatToLastLog("invalid - not enough gold");
                return false;
            }
        }
        unitService.concatToLastLog("wrong command");
        return false;
    }

    public Unit recruit(String unitType) {
        switch (unitType) {
            case "A":
                return new Archer();
            case "H":
                return new Horse();
            case "F":
                return new Warrior();
            case "W":
                return new Worker();
            default:
                unitService.concatToLastLog("- invalid unit name");
                return null;
        }
    }


    public void addUnitsToList(Player player, Owner owner) {
        Unit newUnit = player.getBase().getNewUnit();
        if (newUnit != null && !player.getUnits().contains(newUnit)) {
            newUnit.setOwner(owner);
            player.getUnits().add(newUnit);
        }
    }

    public boolean move(Player player, String unitId, String move) {
        boolean isMoveCorrect = false;
        Optional<Unit> found = getUnit(player, unitId);
        if (found.isPresent()) {
            Unit unit = found.get();
            unitService.getLogs().add(unit.getName());
            unitService.concatToLastLog(unit.getCoordinates().toString());
            if (unit.isEntrench()) {
                unit.setEntrench(false);
                unitService.concatToLastLog("leaves the trench and");
            }
            unitService.concatToLastLog("moves");
            Coordinates unitCoordinates = unit.getCoordinates();
            Coordinates baseCoordinates = player.getBase().getCoordinates();
            if (unit.getActionPoints() == 0) {
                unitService.concatToLastLog("- action invalid - no AP");
                return false;
            }
            switch (move) {
                case "U":
                    isMoveCorrect = unitService.moveUp(unit);
                    break;
                case "D":
                    isMoveCorrect = unitService.moveDown(unit);
                    break;
                case "L":
                    isMoveCorrect = unitService.moveLeft(unit);
                    break;
                case "R":
                    isMoveCorrect = unitService.moveRight(unit);
                    break;
                default:
                    return false;
            }
            if (baseCoordinates.equals(unitCoordinates) && isMoveCorrect) {
                player.getBase().setNewUnit(null);
                unitService.concatToLastLog("BASE is empty");
            }
            if (!isMoveCorrect) {
                unitService.concatToLastLog("- invalid action");
            }
        }
        return isMoveCorrect;
    }

    public boolean attack(Player player, Player enemy, String attackerID, String targetID) {
        Optional<Unit> found = getUnit(player, attackerID);
        if (found.isPresent()) {
            Unit unit = found.get();
            unitService.getLogs().add(unit.getName());
            unitService.concatToLastLog(unit.getCoordinates().toString() + " attacks");
            if (unit.getActionPoints() <= 0) {
                unitService.concatToLastLog("invalid - no AP");
                return false;
            } else {
                Optional<Unit> foundEnemy = getUnit(enemy, targetID);
                if (foundEnemy.isPresent()) {
                    Unit enemyUnit = foundEnemy.get();
                    unitService.concatToLastLog(enemyUnit.getName() + " " + enemyUnit.getCoordinates().toString());
                    boolean isAttacked = unitService.attack(unit, enemyUnit);
                    if (isAttacked && enemyUnit.getHp() <= 0) {
                        if (enemyUnit.getCoordinates().equals(enemy.getBase().getCoordinates())) {
                            enemy.getBase().setNewUnit(null);
                        }
                        enemy.getUnits().remove(enemyUnit);
                    }
                    return isAttacked;
                }
            }
            //Atak na bazę
            if (targetID.equals(enemy.getBase().getId())) {
                unitService.concatToLastLog("enemy BASE");
                return unitService.attack(unit, enemy.getBase());
            }
        }
        return false;
    }

    private Optional<Unit> getUnit(Player player, String unitID) {
        return player.getUnits().stream().filter(unit -> unit.getId().equals(unitID))
                .findFirst();
    }


    public boolean entrench(Player player, String id) {
        Optional<Unit> found = getUnit(player, id);
        if (found.isPresent()) {
            Unit unit = found.get();
            unitService.getLogs().add("entrench " + unit.getName()
                    + " " + unit.getCoordinates().toString());
            return unitService.entrench(unit);
        }
        return false;
    }

    public void updateMiningState(Player player, Owner owner) {
        //logs.add("ms");
        unitService.setWorkersIntoMineCounter(player, owner);
        unitService.earnGoldFromMines(player, owner);
    }

    public List<String> getLogs() {
        return unitService.getLogs();
    }

}
