package com.tg.backend.service;

import com.tg.backend.common.Coordinates;
import com.tg.backend.common.Owner;
import com.tg.backend.model.Base;
import com.tg.backend.model.Mine;
import com.tg.backend.model.Obstacle;
import com.tg.backend.model.Player;
import com.tg.backend.model.units.Unit;
import com.tg.backend.model.units.Worker;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UnitService {

    private MapService mapService;

    public UnitService() {
        this.mapService = new MapService();
    }

    private List<String> logs = new ArrayList<>();

    public List<String> getLogs() {
        return logs;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    public void concatToLastLog(String log) {
        logs.add(logs.get(logs.size() - 1) + " " + log);
        logs.remove(logs.size() - 2);
    }

    public boolean moveUp(Unit unit) {
        concatToLastLog("up");
        return updatePosition(unit, 0, 1);
    }

    public boolean moveDown(Unit unit) {
        concatToLastLog("down");
        return updatePosition(unit, 0, -1);
    }

    public boolean moveRight(Unit unit) {
        concatToLastLog("right");
        return updatePosition(unit, 1, 0);
    }

    public boolean moveLeft(Unit unit) {
        concatToLastLog("left");
        return updatePosition(unit, -1, 0);
    }

    private boolean updatePosition(Unit unit, int x, int y) {
        Coordinates coordinates = unit.getCoordinates();
        int actionPoints = unit.getActionPoints();
        Coordinates updated = new Coordinates(coordinates.getX() + x, coordinates.getY() + y);
        if (validateMove(unit, updated)) {
            unit.setActionPoints(--actionPoints);
            mapService.updateMap(unit, updated);
            unit.setCoordinates(updated);
            concatToLastLog("OK, " + unit.getActionPoints() + " AP");
            return true;
        }
        return false;
    }

    public boolean validateMove(Unit unit, Coordinates newCoordinates) {
        if (!mapService.areCoordinatesOnMap(newCoordinates)) {
            concatToLastLog("off the map");
            return false;
        } else if (!mapService.isFieldEmpty(newCoordinates) && !(unit instanceof Worker)) {
            concatToLastLog("to occupied field");
            return false;
        } else if (!mapService.isFieldEmpty(newCoordinates) && unit instanceof Worker) { //.getName().equals("WORKER")
            for (Mine mine : mapService.getMap().getMines()) {
                if (newCoordinates.equals(mine.getCoordinates())
                        && (mine.getOwner().equals(unit.getOwner()) || mine.getOwner().equals(Owner.NEUTRAL))) {
                    return true;
                }
            }
            concatToLastLog("to occupied field");
            return false;
        }
        return true;
    }

    public boolean attack(Unit unit, Unit target) {
        int damage = unit.getDamage();
        if (target.isEntrench()) {
            concatToLastLog("in the trench");
            damage = damage / 2;
        }
        int actionPoints = unit.getActionPoints();
        int targetHp = target.getHp();
        if (isInRange(unit.getCoordinates(), target.getCoordinates(), unit.getRangeOfAttack())) {
            target.setHp(targetHp - damage);
            unit.setActionPoints(--actionPoints);
            concatToLastLog("OK, " + unit.getActionPoints() + " AP, -" + damage + " HP (left " + target.getHp() + " HP)");
            return true;
        }
        return false;

    }

    public boolean attack(Unit unit, Base target) {
        int damage = unit.getDamage();
        int actionPoints = unit.getActionPoints();
        int targetHp = target.getHp();
        if (isInRange(unit.getCoordinates(), target.getCoordinates(), unit.getRangeOfAttack())) {
            target.setHp(targetHp - damage);
            unit.setActionPoints(--actionPoints);
            concatToLastLog("OK, " + unit.getActionPoints() + " AP, -" + damage + " HP (left " + target.getHp() + " HP)");
            return true;
        }
        return false;
    }

    private boolean isInRange(Coordinates coordinates, Coordinates targetCoordinates, int range) {
        int x1 = coordinates.getX();
        int y1 = coordinates.getY();
        int x2 = targetCoordinates.getX();
        int y2 = targetCoordinates.getY();
        if (x1 == x2 && Math.abs(y1 - y2) <= range) {
            for (int y = Math.min(y1, y2); y <= Math.max(y1, y2); y++) {
                if (mapService.getGameObject(new Coordinates(x1, y)) instanceof Obstacle) {
                    concatToLastLog("invalid - obstacle between units");
                    return false;
                }
            }
            concatToLastLog("vertically");
            return true;

        } else if (y1 == y2 && Math.abs(x1 - x2) <= range) {
            for (int x = Math.min(x1, x2); x <= Math.max(x1, x2); x++) {
                if (mapService.getGameObject(new Coordinates(x, y1)) instanceof Obstacle) {
                    concatToLastLog("invalid - obstacle between units");
                    return false;
                }
            }
            concatToLastLog("horizontally");
            return true;
        } else if (x1 != x2 && y1 != y2) {
            concatToLastLog("invalid direction");
            return false;
        } else {
            concatToLastLog("invalid - range to small");
            return false;
        }
    }

    public boolean entrench(Unit unit) {
        if (unit.getActionPoints() > 0) {
            unit.setEntrench(true);
            unit.setActionPoints(unit.getActionPoints() - 1);
            concatToLastLog("OK, " + unit.getActionPoints() + " AP, ");
            return true;
        } else {
            concatToLastLog("invalid - no AP");
            return false;
        }
    }


    public void setWorkersIntoMineCounter(Player player, Owner owner) {
        int sumInAllMines = 0;
        for (Mine mine : mapService.getMap().getMines()) {
            int counterInOneMine = 0;
            for (Unit unit : player.getUnits()) {
                if (mine.getCoordinates().equals(unit.getCoordinates())) {
                    counterInOneMine++;
                }
                mine.setOwner(counterInOneMine > 0 ? owner : Owner.NEUTRAL);
                mine.setWorkersNumber(counterInOneMine);
            }
            sumInAllMines += counterInOneMine;
        }
        String allWorkers = String.valueOf(sumInAllMines);
        //concatToLastLog(allWorkers + "WORKERS in mines");
    }

    public void earnGoldFromMines(Player player, Owner owner) {
        for (Mine mine : mapService.getMap().getMines()) {
            int resources = mine.getGoldLeft();
            if (mine.getWorkersNumber() > 0 && mine.getOwner().equals(owner)) {
                int income = mine.getWorkersNumber() * mine.getMiningPerWorker();
                player.setGold(player.getGold() + Math.min(income, resources));
                mine.setGoldLeft(Math.max(resources - income, 0));
            }
        }
        String gold = String.valueOf(player.getGold());
        //concatToLastLog(gold + " GOLD");
    }
}



