package com.tg.backend.service;

import com.tg.backend.common.*;
import com.tg.backend.model.*;
import com.tg.backend.repository.GameStateRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;
import java.util.stream.Collectors;

public class GameService {

    private PlayerService playerService;
    private GameState currentGameState;
    private Game game;

    private GameStateRepository gameStateRepository;

    private final String[] mapsPaths = {
            "maps/mapExample1.txt",
            "maps/mapSmall.txt",
            "maps/mapExample2.txt"
    };

    public void initGameParameters(@RequestBody GameData data) {
        game = new Game(data.getGameID(), data.getPlayer1ID(), data.getPlayer2ID());
        innerBodyGameParameters();
    }

    public void initGameParameters(@RequestBody TournamentData data) {
        game = new Game(data.getId(), data.getIdOfFirstPlayer(), data.getIdOfSecondPlayer());
        innerBodyGameParameters();
    }

    private void innerBodyGameParameters() {

        /* USTAWIENIE POCZĄTKOWYCH PARAMETRÓW */
        MapService mapService = new MapService();
        playerService = new PlayerService();

        /* USTAWIENIE MAPY */
        FileMapReader fileMapReader = new FileMapReader(mapsPaths[2]);
        Map map = new Map(fileMapReader);
        mapService.setMap(map);
        game.setMap(map);

        setUpPlayerOwnerTypes();

        setupPlayerBases(map);
        game.setMapPath(mapsPaths[2]);
        game.getPlayer1().setActive(true);
    }

    public void gameLoop(String player1_url, String player2_url) {

        for (; game.getCurrentTurn() < game.getMaxTurns(); game.incrementCurrentTurn()) {
            executeHalfTurn(game, player1_url);

            if (game.checkIfWinner()) break;

            executeHalfTurn(game, player2_url);

            if (game.checkIfWinner()) break;
            if (game.getCurrentTurn() == game.getMaxTurns() - 1) {
                game.setWinnerByUnitsNumber();
            }
        }
    }

    private void executeHalfTurn(Game game, String activePlayerURL) {
        RestTemplate restTemplate = new RestTemplate();
        /*RestTemplate restTemplate = new AppConfig().restTemplate(new RestTemplateBuilder());*/
        try {
            Order playerOrder = restTemplate.postForObject(activePlayerURL, new GameState(game), Order.class);
            playerService.executeOrder(game.getActivePlayer(), game.getInactivePlayer(), playerOrder, game, gameStateRepository);
            deleteDeadUnits(game.getInactivePlayer());
            resetActionPoints(game.getActivePlayer());
            if (playerOrder != null) {
                game.setLastActions(playerOrder.getActions());
            }
            /*game.setLastLogs(playerService.getLogs());*/
            Owner owner = game.getPlayer1().isActive() ? Owner.PLAYER1 : Owner.PLAYER2;
            playerService.updateMiningState(game.getActivePlayer(), owner);
            game.incrementCurrentStep();
            addUnitsToPlayersList();
            gameStateRepository.save(new GameState(game));
            currentGameState = new GameState(game);

            playerService.getLogs().clear();

            game.switchActivePlayer();

        } catch (HttpServerErrorException | NullPointerException e) {
                game.getActivePlayer().getBase().setHp(0);
        }
    }

    public int getWinnerId() {
        return game.getWinnerId();
    }

    private void setupPlayerBases(Map map) {
        game.getPlayer1().setBase(new Base(map.getBases().get(1).getCoordinates(), Owner.PLAYER1));
        game.getPlayer1().getBase().setId(UUID.randomUUID().toString());
        game.getPlayer2().setBase(new Base(map.getBases().get(0).getCoordinates(), Owner.PLAYER2));
        game.getPlayer2().getBase().setId(UUID.randomUUID().toString());
    }

    private void addUnitsToPlayersList() {
        playerService.addUnitsToList(game.getPlayer1(), Owner.PLAYER1);
        playerService.addUnitsToList(game.getPlayer2(), Owner.PLAYER2);
    }

    private void deleteDeadUnits(Player player) {
        player.getUnits().removeIf(unit -> unit.getHp() <= 0);
    }

    private void resetActionPoints(Player player) {
        player.getUnits().stream().map(unit -> {
            unit.resetActionPoints();
            return unit;
        }).collect(Collectors.toList());
    }

    private void setUpPlayerOwnerTypes() {
        game.getPlayer1().setOwner(Owner.PLAYER1);
        game.getPlayer2().setOwner(Owner.PLAYER2);
    }

    public void setGameStateRepository(GameStateRepository gameStateRepository) {
        this.gameStateRepository = gameStateRepository;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public GameState getCurrentGameState() {
        return currentGameState;
    }

    public void setCurrentGameState(GameState currentGameState) {
        this.currentGameState = currentGameState;
    }
}
