package com.tg.backend.service;

import com.tg.backend.common.Coordinates;
import com.tg.backend.model.Base;
import com.tg.backend.model.GameObject;
import com.tg.backend.model.Map;
import com.tg.backend.model.Mine;
import com.tg.backend.model.units.Unit;
import org.springframework.stereotype.Service;

@Service
public class MapService {

    private static Map map;

    public MapService() {
    }

    public MapService(Map map) {
        this.map = map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Map getMap(){
        return map;
    }

    public GameObject getGameObject(Coordinates coordinates) {
        return map.getGameObjects()[coordinates.getX()][coordinates.getY()];
    }

    public GameObject add(GameObject gameObject, Coordinates coordinates) {
        if (isFieldEmpty(coordinates) && areCoordinatesOnMap(coordinates)) {
            map.getGameObjects()[coordinates.getX()][coordinates.getY()] = gameObject;
            return getGameObject(coordinates);
        } else {
            return null;
        }
    }

    public void remove(GameObject gameObject) {
        gameObject = null;
    }

    public boolean isFieldEmpty(Coordinates coordinates) {
        GameObject gameObject = getGameObject(coordinates);
        //jesli obiekt to jednostka z 0 hp, pole jest traktowane jako puste
        if (gameObject == null || (gameObject instanceof Unit && ((Unit) gameObject).getHp() <= 0)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean areCoordinatesOnMap(Coordinates coordinates) {
        return coordinates.getY() >= 0 && coordinates.getY() < map.getHeight() &&
                coordinates.getX() >= 0 && coordinates.getX() < map.getWidth();
    }

    private boolean isMineOrBaseThere(Coordinates coordinates) {
        for (Mine mine : map.getMines()) {
            if (coordinates.equals(mine.getCoordinates())) {
                return true;
            }
        }
        for (Base base : map.getBases()) {
            if (coordinates.equals(base.getCoordinates())) {
                return true;
            }
        }
        return false;
    }

    public void updateMap(GameObject gameObject, Coordinates newCoordinates) {
        GameObject[][] gameObjects = map.getGameObjects();
        Coordinates oldCoordinates = gameObject.getCoordinates();
        for (int x = 0; x < map.getWidth(); x++) {
            for (int y = 0; y < map.getHeight(); y++) {
                if (!isMineOrBaseThere(oldCoordinates) && oldCoordinates.equals(new Coordinates(x,y))) {
                    //jeżeli na obecnym polu nie ma bazy, kopalni
                    //i jest to pole na którym znajdował się gameObject
                    gameObjects[x][y] = null;
                }
                if ( newCoordinates.equals(new Coordinates(x,y))) {
                    gameObjects[x][y] = gameObject;
                }
                if (gameObjects[x][y] instanceof Unit){
                    Unit unit = (Unit) gameObjects[x][y];//?
                    if(unit.getHp()<=0){
                        gameObjects[x][y] = null;
                    }
                }
            }
        }
        map.setGameObjects(gameObjects);
    }
}
