package com.tg.backend.model;

import com.tg.backend.common.Action;
import com.tg.backend.utilities.ReaderXML;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Game {
    private int gameId;
    private int tournamentId;
    private Player player1;
    private Player player2;
    private int currentTurn;
    private int currentStep;
    private int maxTurns;
    private int winnerId;
    private String mapPath;
    private List<Action> lastActions;
    private List<String> lastLogs; // tu będzie przypisana lista logs z PlayerService
    private Map map;

    public Game() {
        this.currentTurn = 0;
    }

    public Game(int gameId, int player1ID, int player2ID) {
        this.currentTurn = 0;
        this.currentStep = 0;
        this.gameId = gameId;
        this.maxTurns = ReaderXML.getIntegerInsideParentTag("game", "maximumTurns");
        player1 = new Player(player1ID);
        player2 = new Player(player2ID);
    }

    public void incrementCurrentTurn() {
        currentTurn = currentTurn + 1;
    }

    public void incrementCurrentStep() {
        currentStep = currentStep + 1;
    }

    public int getWinnerId() {
        return winnerId;
    }

    public boolean checkIfWinner() {
        if (player1.getBase().getHp() <= 0) {
            setWinnerId(player2.getId());
            return true;
        }
        if (player2.getBase().getHp() <= 0) {
            setWinnerId(player1.getId());
            return true;
        }
        return false;
    }

    public void setWinnerByUnitsNumber() {
        if (player1.getUnits().size() == player2.getUnits().size()) {
            if (player1.getBase().getHp() >= player2.getBase().getHp()) {
                setWinnerId(player1.getId());
            } else {
                setWinnerId(player2.getId());
            }
        } else if (player1.getUnits().size() > player2.getUnits().size()) {
            setWinnerId(player1.getId());
        } else {
            setWinnerId(player2.getId());
        }
    }

    public Player getActivePlayer(){
        return player1.isActive() ? player1 : player2;
    }

    public Player getInactivePlayer(){
        return player1.isActive() ? player2 : player1;
    }

    public void switchActivePlayer(){
        player1.setActive(!player1.isActive());
        player2.setActive(!player1.isActive());
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
    }

    public int getMaxTurns() {
        return maxTurns;
    }

    public void setMaxTurns(int maxTurns) {
        this.maxTurns = maxTurns;
    }

    public void setWinnerId(int winnerId) {
        this.winnerId = winnerId;
    }

    @Override
    public String toString() {
        return String.format(
                "Game ID=%s, player1_ID=%s, player2_ID=%s,  MaximumTurns=%d",
                gameId, player1.getId(), player2.getId(), maxTurns);
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getMapPath() {
        return mapPath;
    }

    public void setMapPath(String mapPath) {
        this.mapPath = mapPath;
    }

    public List<Action> getLastActions() {
        return lastActions;
    }

    public void setLastActions(List<Action> lastActions) {
        this.lastActions = lastActions;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }


    public List<String> getLastLogs() {
        return lastLogs;
    }

    public void setLastLogs(List<String> lastLogs) {
        this.lastLogs = lastLogs;
    }
    
    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
}