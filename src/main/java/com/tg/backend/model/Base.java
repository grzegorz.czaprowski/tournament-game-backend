package com.tg.backend.model;

import com.tg.backend.common.Coordinates;
import com.tg.backend.common.Owner;
import com.tg.backend.model.units.Unit;
import com.tg.backend.utilities.ReaderXML;
import org.springframework.stereotype.Component;

@Component
public class Base extends GameObject {

    private int hp;
    private Unit newUnit; // jednostka obecnie budowana w bazie

    public Base() { }

    public Base(Coordinates coordinates, Owner owner) {
        hp = ReaderXML.getIntegerInsideParentTag("base", "hp");
        this.coordinates = coordinates;
        this.owner = owner;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Unit getNewUnit() {
        return newUnit;
    }

    public void setNewUnit(Unit newUnit) {
        this.newUnit = newUnit;
    }
}
