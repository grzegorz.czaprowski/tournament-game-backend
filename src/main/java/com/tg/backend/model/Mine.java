package com.tg.backend.model;

import com.tg.backend.utilities.ReaderXML;

public class Mine extends GameObject {

    private int goldLeft;
    private int workersNumber;
    private int miningPerWorker = 50; // TODO do pliku xml

    public Mine() {
        goldLeft = ReaderXML.getIntegerInsideParentTag("mine", "numberOfResources");
    }

    public int getGoldLeft() {
        return goldLeft;
    }

    public int getWorkersNumber() {
        return workersNumber;
    }

    public void setWorkersNumber(int workersNumber) {
        this.workersNumber = workersNumber;
    }

    public void setGoldLeft(int goldLeft) {
        this.goldLeft = goldLeft;
    }

    public int getMiningPerWorker() {
        return miningPerWorker;
    }
}
