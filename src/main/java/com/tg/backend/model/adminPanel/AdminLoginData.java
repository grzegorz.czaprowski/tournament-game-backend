package com.tg.backend.model.adminPanel;

import java.util.Objects;

public class AdminLoginData {
    private String login, password;

    public AdminLoginData() {
    }

    public AdminLoginData(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdminLoginData that = (AdminLoginData) o;
        return login.equals(that.getLogin())
                && password.equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }
}
