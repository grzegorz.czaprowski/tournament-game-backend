package com.tg.backend.model.adminPanel;

public class AdminUnit {
    private String name;
    private int hp, range, ap, dmg, cost;

    public int getHp() {
        return hp;
    }

    public int getRange() {
        return range;
    }

    public int getAp() {
        return ap;
    }

    public int getDmg() {
        return dmg;
    }

    public int getCost() {
        return cost;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public void setAp(int ap) {
        this.ap = ap;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
