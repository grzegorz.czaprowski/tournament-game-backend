package com.tg.backend.model.units;

import com.tg.backend.utilities.ReaderXML;

public class Archer extends Unit {

    public Archer() {
        getStatisticsFromFile("archer");
        name = "ARCHER";
    }

    @Override
    public void resetActionPoints() {
        actionPoints = ReaderXML.getIntegerInsideParentTag("archer", "ap");
    }
}
