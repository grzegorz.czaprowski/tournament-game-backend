package com.tg.backend.model.units;

import com.tg.backend.utilities.ReaderXML;

public class Horse extends Unit {

    public Horse() {
        getStatisticsFromFile("horse");
        name = "HORSE";
    }

    @Override
    public void resetActionPoints() {
        actionPoints = ReaderXML.getIntegerInsideParentTag("horse", "ap");
    }
}
