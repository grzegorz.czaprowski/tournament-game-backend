package com.tg.backend.model.units;

import com.tg.backend.model.GameObject;
import com.tg.backend.utilities.ReaderXML;

import java.util.Objects;

public abstract class Unit extends GameObject {
    protected int hp;
    protected String name;
    protected int rangeOfAttack;
    protected int actionPoints;
    protected int damage;
    protected boolean isEntrench;
    protected int cost;

    public abstract void resetActionPoints();

    public void getStatisticsFromFile(String unitName){
        hp = ReaderXML.getIntegerInsideParentTag(unitName, "hp");
        rangeOfAttack = ReaderXML.getIntegerInsideParentTag(unitName, "range");
        actionPoints = ReaderXML.getIntegerInsideParentTag(unitName, "ap");
        damage = ReaderXML.getIntegerInsideParentTag(unitName, "damage");
        cost = ReaderXML.getIntegerInsideParentTag(unitName, "cost");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getRangeOfAttack() {
        return rangeOfAttack;
    }

    public int getActionPoints() {
        return actionPoints;
    }

    public void setActionPoints(int actionPoints) {
        this.actionPoints = actionPoints;
    }

    public boolean isEntrench() {
        return isEntrench;
    }

    public void setEntrench(boolean entrench) {
        isEntrench = entrench;
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return id.equals(unit.id) &&
                hp == unit.hp &&
                rangeOfAttack == unit.rangeOfAttack &&
                actionPoints == unit.actionPoints &&
                damage == unit.damage &&
                isEntrench == unit.isEntrench &&
                cost == unit.cost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hp, rangeOfAttack, actionPoints, damage, isEntrench, cost);
    }
}
