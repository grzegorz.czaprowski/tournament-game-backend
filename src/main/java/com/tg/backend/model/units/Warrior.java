package com.tg.backend.model.units;

import com.tg.backend.utilities.ReaderXML;

public class Warrior extends Unit {

    public Warrior() {
        getStatisticsFromFile("warrior");
        name = "WARRIOR";
    }

    @Override
    public void resetActionPoints() {
        actionPoints = ReaderXML.getIntegerInsideParentTag("warrior", "ap");
    }
}
