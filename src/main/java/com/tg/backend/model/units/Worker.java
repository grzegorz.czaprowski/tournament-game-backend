package com.tg.backend.model.units;

import com.tg.backend.utilities.ReaderXML;

public class Worker extends Unit {

    public Worker() {
        getStatisticsFromFile("worker");
        name = "WORKER";
    }

    @Override
    public void resetActionPoints() {
        actionPoints = ReaderXML.getIntegerInsideParentTag("worker", "ap");
    }
}
