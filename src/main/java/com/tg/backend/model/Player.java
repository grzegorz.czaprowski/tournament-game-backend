package com.tg.backend.model;

import com.tg.backend.common.Owner;
import com.tg.backend.model.units.Unit;
import com.tg.backend.utilities.ReaderXML;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int id;
    private int gold;
    private Base base;
    private List<Unit> units;
    private boolean active;
    private Owner owner;

    public Player() {

    }

    public Player(int id) {
        this.id = id;
        this.gold = ReaderXML.getIntegerInsideParentTag("player", "gold");
        this.units = new ArrayList<>();
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public int getId() {
        return id;
    }

    public int getGold() {
        return gold;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Base getBase() {
        return base;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
