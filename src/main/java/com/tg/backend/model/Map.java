package com.tg.backend.model;

import com.tg.backend.common.Coordinates;
import com.tg.backend.common.Owner;

import java.util.ArrayList;
import java.util.List;

public class Map {

    static final int EARTH = 0; // empty
    static final int GRASS = 1; // empty
    static final int WATER = 2; // OBSTACLE
    static final int MOUNTAIN = 3; // OBSTACLE
    static final int BASE = 4;
    static final int MINE = 5;

    private int width;
    private int height;
    private GameObject[][] gameObjects;
    private List<Base> bases = new ArrayList<>();
    private List<Mine> mines = new ArrayList<>();

    public Map(FileMapReader fileMapReader) {
        this.width = fileMapReader.getWidth();
        this.height = fileMapReader.getHeight();
        this.gameObjects = new GameObject[width][height];
        setGameObjectsOnMap(fileMapReader);
    }

    private void setGameObjectsOnMap(FileMapReader fileMapReader) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (fileMapReader.getMapScheme()[height - 1 - y][x] == BASE) {
                    Base base = new Base();
                    base.setCoordinates(new Coordinates(x, y));
                    gameObjects[x][y] = base;
                    bases.add(base);
                } else if (fileMapReader.getMapScheme()[height - 1 - y][x] == WATER
                        || fileMapReader.getMapScheme()[height - 1 - y][x] == MOUNTAIN) {
                    Obstacle obstacle = new Obstacle();
                    obstacle.setCoordinates(new Coordinates(x, y));
                    gameObjects[x][y] = obstacle;
                } else if (fileMapReader.getMapScheme()[height - 1 - y][x] == MINE) {
                    Mine mine = new Mine();
                    mine.setCoordinates(new Coordinates(x, y));
                    mine.setOwner(Owner.NEUTRAL);
                    gameObjects[x][y] = mine;
                    mines.add(mine);
                }
            }
        }
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return height;
    }

    public GameObject[][] getGameObjects() {
        return gameObjects;
    }

    public void setGameObjects(GameObject[][] gameObjects) {
        this.gameObjects = gameObjects;
    }

    public List<Mine> getMines() {
        return mines;
    }

    public List<Base> getBases() {
        return bases;
    }
}
