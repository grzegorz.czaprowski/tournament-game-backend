package com.tg.backend.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileMapReader {
    private static final int NUMBER_OF_BASES = 2;

    private int width;
    private int height;
    private int[][] mapScheme;

    public FileMapReader(String mapPath) {
        /*this.mapScheme = getTiledMapFromFile(new File(mapPath));*/
        File file = new File(this.getClass().getClassLoader().getResource(mapPath)
                        .getPath().replaceAll("%20"," "));
        this.mapScheme = correctIfNecessary(getTiledMapFromFile(file));
    }

    public int[][] getMapScheme() {
        return mapScheme;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[][] createEmptyTiledMap(File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                height++;
                String[] partsOfLine = line.split(" ");
                width = partsOfLine.length;
            }
            return new int[height][width];
        } catch (Exception e) {
            /*System.err.println("error przy wczytywaniu");*/
            e.printStackTrace();
        }
        return null;
    }

    public int[][] getTiledMapFromFile(File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            int[][] tiledMap = createEmptyTiledMap(file);
            for (int y = 0; y < height; y++) {
                if ((line = br.readLine()) != null) {
                    String[] partsOfLine = line.split(" ");
                    int tiles[] = new int[partsOfLine.length];
                    for (int x = 0; x < partsOfLine.length; x++) {
                        tiles[x] = Integer.parseInt(partsOfLine[x]);
                    }
                    tiledMap[y] = tiles;
                }
            }
            return tiledMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int[][] correctIfNecessary(int[][] tiledMap) {
        if (!isSymmetrical(tiledMap)) {
            tiledMap = changeToSymmetrical(tiledMap);
        }
        if (!isNumberOfBasesOk(tiledMap)) {
            tiledMap = adjustBases(tiledMap);
        }
        return tiledMap;
    }

    /* SPRAWDZANIE I KOREKTA SYMETRII */
    public boolean isSymmetrical(int[][] tiledMap) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (tiledMap[y][x] != tiledMap[height - y - 1][width - x - 1]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int[][] changeToSymmetrical(int[][] tiledMap) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tiledMap[height - y - 1][width - x - 1] = tiledMap[y][x];
            }
        }
        return tiledMap;
    }

    /* SPRAWDZENIE I KOREKTA BAZ */
    public int countBases(int[][] tiledMap) {
        int basesCounter = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (tiledMap[y][x] == Map.BASE) {
                    basesCounter++;
                }
            }
        }
        return basesCounter;
    }
    public boolean isNumberOfBasesOk(int[][] tiledMap) {
        if (countBases(tiledMap) != NUMBER_OF_BASES) {
            return false;
        }
        return true;
    }

    public int[][] adjustBases(int[][] tiledMap) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (tiledMap[y][x] == Map.BASE) {
                    tiledMap[y][x] = Map.EARTH;
                }
            }
        }
        tiledMap[0][0] = Map.BASE;
        tiledMap[height - 1][width - 1] = Map.BASE;
        return tiledMap;
    }
}