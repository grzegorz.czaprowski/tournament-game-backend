package com.tg.backend.model;

import com.tg.backend.common.Coordinates;
import com.tg.backend.common.Owner;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GameObject {

    protected String id;
    protected Coordinates coordinates;
    protected Owner owner;

    public GameObject() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

}
