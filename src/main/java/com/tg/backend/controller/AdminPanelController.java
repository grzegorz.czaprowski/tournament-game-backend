package com.tg.backend.controller;

import com.mongodb.*;
import com.tg.backend.model.adminPanel.AdminLoginData;
import com.tg.backend.model.adminPanel.AdminUnit;
import com.tg.backend.utilities.ReaderXML;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "*")
public class AdminPanelController {

    @PostMapping("/sign-in")
    public ResponseEntity<AdminLoginData> login(@RequestBody AdminLoginData loginDataToValidate) {
        MongoClient mongoClient = new MongoClient("hotdev.ncdc");
        DB db = mongoClient.getDB("game_mongo_db");
        DBCollection collection = db.getCollection("admin");
        DBCursor cursor = collection.find();

        while (cursor.hasNext()) {
            DBObject dbObject = cursor.next();
            String loginFromDB = (String) dbObject.get("login");
            String passwordFromDB = (String) dbObject.get("password");
            AdminLoginData adminDataFromDB = new AdminLoginData(loginFromDB, passwordFromDB);
            if (adminDataFromDB.equals(loginDataToValidate)) {
                //TODO: Dodac sesje
                return new ResponseEntity<>(loginDataToValidate, HttpStatus.OK);
            }
        }
        return null;
    }

    @GetMapping("/get-unit-params")
    public ResponseEntity<AdminUnit[]> getUnitParams(){
        AdminUnit[] units = new AdminUnit[4];
        for(int i = 0; i < units.length; i++) { units[i] = new AdminUnit();}
        units[0].setName("worker");
        units[1].setName("warrior");
        units[2].setName("archer");
        units[3].setName("horse");


        for(AdminUnit unit: units){
            unit.setAp(ReaderXML.getIntegerInsideParentTag(unit.getName(), "ap"));
            unit.setHp(ReaderXML.getIntegerInsideParentTag(unit.getName(), "hp"));
            unit.setCost(ReaderXML.getIntegerInsideParentTag(unit.getName(), "cost"));
            unit.setDmg(ReaderXML.getIntegerInsideParentTag(unit.getName(), "damage"));
            unit.setRange(ReaderXML.getIntegerInsideParentTag(unit.getName(), "range"));
        }
        return new ResponseEntity<>(units, HttpStatus.OK);
    }

    @GetMapping("/get-mining-params")
    public ResponseEntity<Integer[]> getMiningParams(){
        int resources, miningPerTurn;
        resources = ReaderXML.getIntegerInsideParentTag("mine", "numberOfResources");
        miningPerTurn = ReaderXML.getIntegerInsideParentTag("mine", "miningPerTurn");
        return new ResponseEntity<>(new Integer[] {resources, miningPerTurn}, HttpStatus.OK);
    }

    @PostMapping("/edit-unit-params")
    public void editUnitParams(@RequestBody AdminUnit[] unitsParams){
        for(AdminUnit unit : unitsParams){
            ReaderXML.setIntegerInsideParentTag("/game/units/", unit.getName(), "hp", unit.getHp());
            ReaderXML.setIntegerInsideParentTag("/game/units/", unit.getName(), "ap", unit.getAp());
            ReaderXML.setIntegerInsideParentTag("/game/units/", unit.getName(), "range", unit.getRange());
            ReaderXML.setIntegerInsideParentTag("/game/units/", unit.getName(), "damage", unit.getDmg());
            ReaderXML.setIntegerInsideParentTag("/game/units/", unit.getName(), "cost", unit.getCost());
        }
    }

    @PostMapping("/edit-mining-params")
    public void getMiningParams(@RequestBody Integer[] miningParams){
        ReaderXML.setIntegerInsideParentTag("/game/","mine", "numberOfResources", miningParams[0]);
        ReaderXML.setIntegerInsideParentTag("/game/","mine", "miningPerTurn", miningParams[1]);
    }

    @PostMapping("/change-password")
    public ResponseEntity<Integer> changePassword(@RequestBody String[] passwords){
        String oldPassword = passwords[0];
        String newPassword = passwords[1];

        MongoClient mongoClient = new MongoClient("hotdev.ncdc");
        DB db = mongoClient.getDB("game_mongo_db");
        DBCollection collection = db.getCollection("admin");
        DBCursor cursor = collection.find();

        while (cursor.hasNext()) {
            DBObject dbObject = cursor.next();
            String passwordFromDB = (String) dbObject.get("password");
            if (passwordFromDB.equals(oldPassword)) {
                dbObject.put("password", newPassword);
                collection.update(new BasicDBObject().append("password", oldPassword), dbObject);
                return new ResponseEntity<>(1, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(0, HttpStatus.OK);
    }
}


