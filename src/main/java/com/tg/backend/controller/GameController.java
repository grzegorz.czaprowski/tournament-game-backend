package com.tg.backend.controller;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.tg.backend.common.*;
import com.tg.backend.model.FileMapReader;
import com.tg.backend.repository.ArchiveGamesDataRepository;
import com.tg.backend.repository.GameStateRepository;
import com.tg.backend.repository.ResultRepository;
import com.tg.backend.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GameController {

    @Autowired
    private GameStateRepository gameStateRepository;

    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private ArchiveGamesDataRepository archiveGamesDataRepository;

    private final String[] mapsPaths = {
            "maps/mapExample1.txt",
            "maps/mapSmall.txt",
            "maps/mapExample2.txt"
    };

    private List<GameService> activeGameServices = new ArrayList<>();

    @CrossOrigin(origins = "*")
    @GetMapping("/getActiveGames")
    public ResponseEntity<List<Integer>> getActiveGames() {
        List<Integer> games = new ArrayList<>();
        activeGameServices.forEach(activeGame -> games.add(activeGame.getGame().getGameId()));
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getUniqueGames")
    public ResponseEntity<List<Integer>> getUniqueGames() {
        List<Integer> gameList = getGameListFromDatabase();
        return new ResponseEntity<>(gameList, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getGameStates/{gameId}")
    public ResponseEntity<List<GameState>> getGameStateFromDB(@PathVariable("gameId") int gameId) {
        List<GameState> gameStateList = gameStateRepository.getByGameId(gameId);
        return new ResponseEntity<>(gameStateList, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/newTournamentGame")
    public void createGame(@RequestBody TournamentData data) {
        GameService gameService = new GameService();
        gameService.setGameStateRepository(gameStateRepository);
        archiveGamesDataRepository.save(new ArchiveGame(data.getId(), getBotURL(data.getIdOfFirstPlayer()),
                getBotURL(data.getIdOfSecondPlayer())));
        gameService.initGameParameters(data);

        activeGameServices.add(gameService);
        try{
            gameService.gameLoop(getBotURL(data.getIdOfFirstPlayer()), getBotURL(data.getIdOfSecondPlayer()));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            resultRepository.save(new GameResult(data.getId(), gameService.getGame().getWinnerId()));
            activeGameServices.remove(gameService);
        }
        // TODO: response to tournament server
        resultRepository.save(new GameResult(data.getId(), gameService.getGame().getWinnerId()));
        sendResultToTournament(new TournamentResult(data.getId(),
                data.getId(), gameService.getGame().getWinnerId()));
        activeGameServices.remove(gameService);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/newGame")
    public ResponseEntity<String> createGame(@RequestBody GameData data) {
        GameService gameService = new GameService();
        gameService.setGameStateRepository(gameStateRepository);
        archiveGamesDataRepository.save(new ArchiveGame(data.getGameID(), data.getPlayer1Url(), data.getPlayer2Url()));
        gameService.initGameParameters(data);

        activeGameServices.add(gameService);
        try {
            GameState initialGameState = new GameState(gameService.getGame());
            //gameStateRepository.save(initialGameState);
            gameService.setCurrentGameState(initialGameState);
            gameService.gameLoop(data.getPlayer1Url(), data.getPlayer2Url());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            resultRepository.save(new GameResult(data.getGameID(), gameService.getGame().getWinnerId()));
            activeGameServices.remove(gameService);
        }

        resultRepository.save(new GameResult(data.getGameID(), gameService.getGame().getWinnerId()));
        activeGameServices.remove(gameService);

        return new ResponseEntity<>("Finished! WIN: " + gameService.getWinnerId(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getCurrentGameState/{gameId}")
    public ResponseEntity<GameState> getGameState(@PathVariable("gameId") int gameId) {
        int turn, step;
        if(activeGameServices.size() != 0) {
            GameService gameService = null;
            for(GameService gs : activeGameServices) {
                if(gs.getGame().getGameId() == gameId) {
                    gameService = gs;
                    break;
                }
            }
            if((gameService != null ? gameService.getCurrentGameState() : null) != null) {
                turn = gameService.getCurrentGameState().getCurrentTurn();
                step = gameService.getCurrentGameState().getCurrentStep();

                List<GameState> gameStateList = gameStateRepository.getByGameIdAndCurrentTurn(gameId, turn, step);
                GameState gameState = null;
                if(gameStateList.size() - 1 >= 0){
                    gameState = gameStateList.get(gameStateList.size()-1);
                }
                return new ResponseEntity<>(gameState, HttpStatus.OK);
            }
        }
        return null;
    }

    @CrossOrigin("*")
    @GetMapping("/getResultGame/{gameId}")
    public ResponseEntity<GameResult> getResultGame(@PathVariable("gameId") int gameId) {
        GameResult gameResult = resultRepository.getByGameId(gameId);
        return new ResponseEntity<>(gameResult, HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getMap")
    public ResponseEntity<int[][]> getMap() {
        return new ResponseEntity<>(new FileMapReader(mapsPaths[2]).getMapScheme(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getGameSettings")
    public ResponseEntity<GameSettings> getGameSettings() {
        return new ResponseEntity<>(new GameSettings(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getArchiveGames")
    public ResponseEntity<List<ArchiveGame>> getArchiveGames() {
        List<ArchiveGame> archiveGames = archiveGamesDataRepository.findAll();
        return new ResponseEntity<>(archiveGames, HttpStatus.OK);
    }

    private void sendResultToTournament(TournamentResult tournamentResult) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject("http://hotdev.ncdc:8080/tournament/game/finish",
                tournamentResult, String.class);
    }

    private List<Integer> getGameListFromDatabase() {
        MongoClient mongoClient = new MongoClient("hotdev.ncdc");
        MongoDatabase mongoDb = mongoClient.getDatabase("game_mongo_db");
        MongoCursor<Integer> mongoCursor = mongoDb.getCollection("game_state")
                .distinct("gameId", Integer.class).iterator();
        List<Integer> gameList = new ArrayList<>();
        while (mongoCursor.hasNext()) {
            gameList.add(mongoCursor.next());
        }
        mongoCursor.close();
        mongoClient.close();
        return gameList;
    }

    private String getBotURL(int playerID) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(
                "http://hotdev.ncdc:8080/usrmgmt/user/bot-url/" + playerID, String.class, String.class);
    }
}
