package com.tg.backend.repository;

import com.tg.backend.common.ArchiveGame;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArchiveGamesDataRepository extends MongoRepository<ArchiveGame, Integer> {
}
