package com.tg.backend.repository;

import com.tg.backend.common.GameResult;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends MongoRepository<GameResult, String> {

    GameResult getByGameId(int gameId);

}
