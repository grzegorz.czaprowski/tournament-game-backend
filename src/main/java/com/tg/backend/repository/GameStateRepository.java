package com.tg.backend.repository;

import com.tg.backend.common.GameState;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameStateRepository extends MongoRepository<GameState, String> {

    List<GameState> getByGameId(int gameId);
    List<GameState> getByGameIdAndCurrentTurn(int gameId, int currentTurn, int currentStep);

}
