package com.tg.backend.common;

public class Action {

    private String id;
    private ActionType actionType;
    private String target;

    public Action() {
    }

    public Action(String id, ActionType actionType, String target) {
        this.id = id;
        this.actionType = actionType;
        this.target = target;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
