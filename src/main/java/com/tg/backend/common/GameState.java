package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tg.backend.model.Game;
import com.tg.backend.model.GameObject;
import com.tg.backend.model.Mine;
import com.tg.backend.model.Player;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Document(collection = "game_state")
public class GameState {

    @JsonProperty("mapPath")
    private String mapPath;
    @JsonProperty("gameId")
    private int gameId;
    @JsonProperty("tournamentId")
    private int tournamentId;
    @JsonProperty("currentTurn")
    private int currentTurn;
    @JsonProperty("currentStep")
    private int currentStep;
    @JsonProperty("player1")
    private Player player1;
    @JsonProperty("player2")
    private Player player2;
    @JsonProperty("mines")
    private List<Mine> mines;
    @JsonProperty("lastActions")
    private List<Action> lastActions;
    @JsonProperty("lastLogs")
    private List<String> lastLogs;

    public GameState() {
    }

    public GameState(Game game) {
        currentTurn = game.getCurrentTurn();
        currentStep = game.getCurrentStep();
        gameId = game.getGameId();
        tournamentId = game.getTournamentId();
        mapPath = game.getMapPath();
        player1 = game.getPlayer1();
        player2 = game.getPlayer2();
        lastActions = game.getLastActions();
        lastLogs = game.getLastLogs();
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public int getCurrentStep() {
        return currentStep;
    }
}