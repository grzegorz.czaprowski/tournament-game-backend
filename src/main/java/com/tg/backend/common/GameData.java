package com.tg.backend.common;

public class GameData {

    private int tournamentID;
    private int gameID;
    private int player1ID;
    private int player2ID;
    private String player1Url;
    private String player2Url;
    private String responseUrl;
    private int winnerID;

    public int getTournamentID() {
        return tournamentID;
    }

    public void setTournamentID(int tournamentID) {
        this.tournamentID = tournamentID;
    }

    public int getGameID() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public int getPlayer1ID() {
        return player1ID;
    }

    public void setPlayer1ID(int player1ID) {
        this.player1ID = player1ID;
    }

    public int getPlayer2ID() {
        return player2ID;
    }

    public void setPlayer2ID(int player2ID) {
        this.player2ID = player2ID;
    }

    public String getPlayer1Url() {
        return player1Url;
    }

    public void setPlayer1Url(String player1Url) {
        this.player1Url = player1Url;
    }

    public String getPlayer2Url() {
        return player2Url;
    }

    public void setPlayer2Url(String player2Url) {
        this.player2Url = player2Url;
    }

    public String getResponseUrl() {
        return responseUrl;
    }

    public void setResponseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
    }

    public int getWinnerID() {
        return winnerID;
    }

    public void setWinnerID(int winnerID) {
        this.winnerID = winnerID;
    }
}
