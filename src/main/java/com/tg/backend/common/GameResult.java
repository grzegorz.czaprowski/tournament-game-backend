package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "results")
public class GameResult {

    @JsonProperty("gameId")
    private int gameId;
    @JsonProperty("winnerId")
    private int winnerId;

    public GameResult() {
    }

    public GameResult(int gameId, int winnerId) {
        this.gameId = gameId;
        this.winnerId = winnerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(int winnerId) {
        this.winnerId = winnerId;
    }
}
