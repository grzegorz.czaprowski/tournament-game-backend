package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tg.backend.model.units.*;
import com.tg.backend.utilities.ReaderXML;

public class GameSettings {
    @JsonProperty("worker")
    private Worker worker;
    @JsonProperty("warrior")
    private Warrior warrior;
    @JsonProperty("archer")
    private Archer archer;
    @JsonProperty("horse")
    private Horse horse;
    @JsonProperty("miningPerTurn")
    int miningPerTurn;
    @JsonProperty("numberOfResources")
    int numberOfResources;

    public GameSettings() {
        worker = new Worker();
        warrior = new Warrior();
        archer = new Archer();
        horse = new Horse();

        numberOfResources = ReaderXML.getIntegerInsideParentTag("mine", "numberOfResources");
        miningPerTurn = ReaderXML.getIntegerInsideParentTag("mine", "miningPerTurn");
    }
}
