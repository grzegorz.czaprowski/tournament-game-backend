package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ArchiveGame {
    @JsonProperty
    private Date date;
    @JsonProperty
    private String urlPlayer1, urlPlayer2;
    @JsonProperty
    private int gameId;

    public ArchiveGame(int gameId, String urlPlayer1, String urlPlayer2) {
        this.gameId = gameId;
        date = new Date();
        this.urlPlayer1 = urlPlayer1;
        this.urlPlayer2 = urlPlayer2;
    }
}
