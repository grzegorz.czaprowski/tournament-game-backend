package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TournamentData {

    @JsonProperty("gameId")
    private Integer gameId;
    @JsonProperty("gameType")
    private Integer gameType;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("idOfFirstPlayer")
    private Integer idOfFirstPlayer;
    @JsonProperty("idOfSecondPlayer")
    private Integer idOfSecondPlayer;
    @JsonProperty("idStatistics")
    private Integer idStatistics;
    @JsonProperty("idWinner")
    private Integer idWinner;
    @JsonProperty("parentIndex")
    private Integer parentIndex;
    @JsonProperty("tournamentId")
    private Integer tournamentId;
    @JsonProperty("whichRound")
    private Integer whichRound;

    public TournamentData() {
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getIdOfFirstPlayer() {
        return idOfFirstPlayer;
    }

    public void setIdOfFirstPlayer(Integer idOfFirstPlayer) {
        this.idOfFirstPlayer = idOfFirstPlayer;
    }

    public Integer getIdOfSecondPlayer() {
        return idOfSecondPlayer;
    }

    public void setIdOfSecondPlayer(Integer idOfSecondPlayer) {
        this.idOfSecondPlayer = idOfSecondPlayer;
    }

    public Integer getId() {
        return id;
    }
}
