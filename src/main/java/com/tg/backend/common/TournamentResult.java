package com.tg.backend.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TournamentResult {

    @JsonProperty("idGame")
    int idGame;
    @JsonProperty("idStatistics")
    int idStatatistics;
    @JsonProperty("idWinner")
    int idWinner;

    public TournamentResult() {
    }

    public TournamentResult(int id, int idStatatistics, int idWinner) {
        this.idGame = id;
        this.idStatatistics = idStatatistics;
        this.idWinner = idWinner;
    }
}
