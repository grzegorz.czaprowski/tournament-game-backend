package com.tg.backend.common;

public enum ActionType {
	ATTACK, RECRUIT, MOVE, ENTRENCH
}