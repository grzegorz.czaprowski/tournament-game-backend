package com.tg.backend.utilities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;

public class ReaderXML {
    private static final String GAME_SETTING_PATH = "/opt/tomcat/webapps/TG_backend_2/WEB-INF/classes/GameSettings.xml";
    //private static final String GAME_SETTING_PATH = "src/main/resources/GameSettings.xml";

    private static Document getDocument(){
        File file = new File(GAME_SETTING_PATH);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        Document document = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    public static String getStringByTagName(String tagName){
        return getDocument().getElementsByTagName(tagName).item(0).getTextContent();
    }

    public static String getStringInsideParentTag(String parentTagName, String tagName){
        Element parentTag = (Element) getDocument().getElementsByTagName(parentTagName).item(0);
        return (parentTag).getElementsByTagName(tagName).item(0).getTextContent();
    }

    public static Integer getIntegerByTagName(String tagName){
        return Integer.valueOf(getDocument().getElementsByTagName(tagName).item(0).getTextContent());
    }

    public static Integer getIntegerInsideParentTag(String parentTagName, String tagName){
        Element parentTag = (Element) getDocument().getElementsByTagName(parentTagName).item(0);
        return Integer.valueOf((parentTag).getElementsByTagName(tagName).item(0).getTextContent());
    }

    public static void setIntegerInsideParentTag(String pathToParent, String parentTagName, String tagName, Integer value){
        Document doc = getDocument();
        XPath xPath = XPathFactory.newInstance().newXPath();
        Node node = null;
        try {
            node = (Node) xPath.compile(pathToParent + parentTagName + "/" + tagName)
                    .evaluate(doc, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        node.setTextContent(Integer.toString(value));

        Transformer tf = null;
        try {
            tf = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        tf.setOutputProperty(OutputKeys.METHOD, "xml");
        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        DOMSource domSource = new DOMSource(doc);
        StreamResult sr = new StreamResult(new File(GAME_SETTING_PATH));
        try {
            tf.transform(domSource, sr);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
